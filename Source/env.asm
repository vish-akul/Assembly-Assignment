extern printf
extern getenv
extern environ 

section .data
	outcl: db "%s",10,0
	out: db "The value of the given environment variable is: %s",10,0
	result: db 0
	env: db 0
	hold: db "",0

section .text
	global main

	main:
	push ebp
	mov ebp,esp

	mov eax, DWORD [ebp+0xc]
	add eax, 0x4
	cmp DWORD [eax],0
	je L1
	push DWORD [eax]
	call getenv
	push eax
	push out
	call printf
	leave
	ret
	
	L1:
	mov ebx, DWORD [environ]
	sub ebx,0x4
	L2:
	add ebx, 0x4
	mov edx, DWORD [ebx]
	push edx
	push outcl
	call printf
	cmp DWORD [ebx], 0x0 
	jne L2

	leave
	ret
