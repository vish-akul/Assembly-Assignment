#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char *argv[]) {
	
	int rot;
	if (getenv("rot")==NULL) {
		rot = 13;
	} else {
		rot = atoi(getenv("rot"));
	}
	
	char *s = argv[1];
	int hold;
	int l = strlen(s);
	for(int i = 0; i < l; i++) {
		if(s[i]>96&&s[i]<123) {
			hold = s[i];
			hold += rot;
			if(hold>122) {
				hold-=26;
				s[i]=hold;
			}
			s[i]=hold;
		}
		else if(s[i]>64&&s[i]<91) {
			hold = s[i];
			hold += rot;
			if(hold>91) {
				hold-=26;
				s[i]=hold;
			}
			s[i]=hold;
		} 
	
	}
	printf("%s\n",s);

	return 0;
}
