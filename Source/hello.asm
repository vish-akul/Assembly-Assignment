extern printf
extern scanf

section .data
	request: db "Hi, What is your name?",10,0
	out: db "%s",0
	greet: db "Hello, %s",10,0
	name: db 0 

section .text
	global main

	main:
	push ebp
	mov ebp, esp
	
	push request
	push out
	call printf
	
	push name
	push out
	call scanf

	push name
	push greet
	call printf
	
	leave
	ret
