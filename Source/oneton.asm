extern printf
extern scanf

section .data
	request: db "Please enter a number:",10,0
	out: db "%s",0
	outnum: db "%d",0
	num: dd 0
	finalout: db "The Sum of the numbers 1 to %d is %d",10,0

section .text
	global main

	main:
	push ebp
	mov ebp, esp

	push request
	push out
	call printf

	push num
	push outnum
	call scanf

	mov ecx,0
	mov ebx, dword [num]
	L1:
	add ecx,ebx
	sub ebx,1
	cmp ebx,0
	jg L1
	
	mov ebx, dword [num]
	push ecx
	push ebx
	push finalout
	call printf

	leave
	ret
