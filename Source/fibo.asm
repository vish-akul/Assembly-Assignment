extern printf
extern atoi

section .data
	in: db "%d",10,0
	N: dd 0
	count: dd 0, 0
	value: dd 0
	value2: dd 0,0

section .text
	global main

	main:
	push ebp
	mov ebp, esp

	
	mov eax , DWORD [ebp+0xc]
	add eax ,0x4
	mov eax ,DWORD [eax]
	push eax
	call atoi
	mov DWORD [N] , eax


	mov ecx, 1
	l1:
	mov ebx, DWORD [value2]
	add ecx, ebx
	mov DWORD [value], ebx
	mov DWORD [value2], ecx
	push ecx
	push in
	call printf
	mov ecx, DWORD [value]
	add DWORD[count], 1
	mov eax, DWORD [count]
	cmp DWORD [N], eax
	jne l1

	leave
	ret

	
